using System.IO;
using System.Threading.Tasks;
using Liquid.Encryption.Pgp.Helpers;
using Xunit;

namespace Liquid.Encryption.Pgp.Tests
{
    public class PgpEncryptionTests
    {
        private const string PrivateKeyFilePath = "/Keys/TestPrivateKey.asc";
        private const string PublicKeyFilePath = "/Keys/TestPublicKey.asc";
        private const string Password = "Test";

        [Fact]
        public async Task TestPgpEncryptDecrypt()
        {
            var fullPrivateKeyPath = Path.Combine(IOHelper.BasePath + PrivateKeyFilePath);
            var fullPublicKeyPath = Path.Combine(IOHelper.BasePath + PublicKeyFilePath);

            using var pgp = new PgpEncryption(fullPublicKeyPath, fullPrivateKeyPath, Password);

            var contentToEncrypt = "some test message";

            var encryptedContent = await pgp.EncryptAsync(contentToEncrypt);

            Assert.NotEqual(contentToEncrypt, encryptedContent);

            var decryptedContent = await pgp.DecryptAsync(encryptedContent);

            Assert.Equal(contentToEncrypt, decryptedContent);
        }
    }
}
