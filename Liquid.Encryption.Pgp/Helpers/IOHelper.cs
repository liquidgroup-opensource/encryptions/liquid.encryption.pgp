﻿using System.IO;
using System.Reflection;
using System.Text;

namespace Liquid.Encryption.Pgp.Helpers
{
    public static class IOHelper
    {
        public static readonly string BasePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        public static MemoryStream GetStream(string stringData)
        {
            var bytes = Encoding.UTF8.GetBytes(stringData);
            return new MemoryStream(bytes);
        }

        public static string GetString(MemoryStream inputStream)
        {
            return Encoding.UTF8.GetString(inputStream.ToArray());
        }

        public static string GetString(Stream inputStream)
        {
            var bufferStream = new MemoryStream();
            inputStream.CopyTo(bufferStream);
            return Encoding.UTF8.GetString(bufferStream.ToArray());
        }
    }
}
