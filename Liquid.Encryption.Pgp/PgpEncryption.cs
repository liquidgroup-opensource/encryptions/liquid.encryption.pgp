﻿using System;
using System.IO;
using System.Threading.Tasks;
using Liquid.Encryption.Pgp.Helpers;
using Org.BouncyCastle.Bcpg;
using PgpCore;

namespace Liquid.Encryption.Pgp
{
    public class PgpEncryption : IPgpEncryption
    {
        private readonly PGP _pgp;

        #region constructor(s) 
        /// <summary>
        /// Create new Instance <see cref="PgpEncryption"/>
        /// </summary>
        /// <remarks>Encrypt and Sign Verify</remarks>
        /// <param name="publicKeyFilePath">the public key path. </param>
        /// <param name="privateKeyFilePath">the private key path.</param>
        /// <param name="password">the private key password.</param>
        public PgpEncryption(string publicKeyFilePath, string privateKeyFilePath, string password)
        {
            if (string.IsNullOrEmpty(publicKeyFilePath)) throw new ArgumentNullException("cannot be null or empty", nameof(publicKeyFilePath));
            if (string.IsNullOrEmpty(privateKeyFilePath)) throw new ArgumentNullException("cannot be null or empty", nameof(privateKeyFilePath));
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException("cannot be null or empyt", nameof(password));

            var keys = new EncryptionKeys(publicKeyFilePath, privateKeyFilePath, password);
            _pgp = CreatePgp(keys);
        }


        /// <summary>
        /// Create new instance of <see cref="publicKeyFilePath"/>
        /// </summary>
        /// <remark>Only for public key encryption</remarks>
        /// <param name="publicKeyFilePath"></param>
        public PgpEncryption(string publicKeyFilePath)
        {
            var keys = new EncryptionKeys(publicKeyFilePath);
            _pgp = CreatePgp(keys);
        }
        #endregion

        #region public methods
        public PgpEncryption(string privateKeyFilePath, string password)
        {
            var keys = new EncryptionKeys(privateKeyFilePath, password);
            _pgp = CreatePgp(keys);
        }

        public async Task<Stream> EncryptAsync(Stream inputStream)
        {
            var outputStream = new MemoryStream();

            await _pgp.EncryptStreamAsync(inputStream, outputStream);

            return outputStream;
        }

        public async Task<string> EncryptAsync(string inputString)
        {
            var streamResult = await EncryptAsync(IOHelper.GetStream(inputString));
            return IOHelper.GetString((MemoryStream)streamResult);
        }

        public async Task<Stream> EncryptAndSignAsync(Stream inputStream)
        {
            var outputStream = new MemoryStream();

            await _pgp.EncryptStreamAndSignAsync(inputStream, outputStream);

            return outputStream;
        }

        public async Task<string> EncryptAndSignAsync(string inputString)
        {
            var streamResult = await EncryptAndSignAsync(IOHelper.GetStream(inputString));
            return IOHelper.GetString((MemoryStream)streamResult);
        }

        public async Task<Stream> DecryptAsync(Stream inputStream)
        {
            var outputStream = new MemoryStream();
            await _pgp.DecryptStreamAsync(inputStream, outputStream);

            return outputStream;
        }

        public async Task<string> DecryptAsync(string inputString)
        {
            var streamResult = await DecryptAsync(IOHelper.GetStream(inputString));
            return IOHelper.GetString((MemoryStream)streamResult);
        }

        public async Task<Stream> DecryptAndVerifyAsync(Stream inputStream)
        {
            var outputStream = new MemoryStream();
            await _pgp.DecryptStreamAndVerifyAsync(inputStream, outputStream);

            return outputStream;
        }

        public async Task<string> DecryptAndVerifyAsync(string inputString)
        {
            var streamResult = await DecryptAndVerifyAsync(IOHelper.GetStream(inputString));
            return IOHelper.GetString((MemoryStream)streamResult);
        }

        public async Task<bool> VerifyAsync(Stream inputStream)
        {
            var verificationResult = await _pgp.VerifyStreamAsync(inputStream);
            return verificationResult;
        }

        public async Task<bool> VerifyAsync(string inputString)
        {
            var verificationResult = await VerifyAsync(IOHelper.GetStream(inputString));
            return verificationResult;
        }

        public async Task<Stream> SignAsync(Stream inputStream)
        {
            var result = new MemoryStream();
            await _pgp.SignStreamAsync(inputStream, result, false);
            result.Position = 0;
            return result;
        }

        public async Task<string> SignAsync(string inputString)
        {
            var streamResult = await SignAsync(IOHelper.GetStream(inputString));
            return IOHelper.GetString(streamResult);
        }

        public void Dispose()
        {
            _pgp?.Dispose();
        }
        #endregion

        #region private methods

        private PGP CreatePgp(IEncryptionKeys keys)
        {
            return new PGP(keys)
            {
                CompressionAlgorithm = CompressionAlgorithmTag.Zip,
                HashAlgorithmTag = HashAlgorithmTag.Sha512,
                SymmetricKeyAlgorithm = SymmetricKeyAlgorithmTag.Aes256
            };
        }
        #endregion
    }
}
