﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Liquid.Encryption.Pgp
{
    public interface IPgpEncryption : IDisposable
    {
        Task<Stream> DecryptAndVerifyAsync(Stream inputStream);
        Task<string> DecryptAndVerifyAsync(string inputString);
        Task<Stream> DecryptAsync(Stream inputStream);
        Task<string> DecryptAsync(string inputString);
        Task<Stream> EncryptAndSignAsync(Stream inputStream);
        Task<string> EncryptAndSignAsync(string inputString);
        Task<Stream> EncryptAsync(Stream inputStream);
        Task<string> EncryptAsync(string inputString);
        Task<Stream> SignAsync(Stream inputStream);
        Task<string> SignAsync(string inputString);
        Task<bool> VerifyAsync(Stream inputStream);
        Task<bool> VerifyAsync(string inputString);
    }
}