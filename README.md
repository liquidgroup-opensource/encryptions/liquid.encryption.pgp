  
  [PGP Encryption WIKI](https://en.wikipedia.org/wiki/Pretty_Good_Privacy)
  
  This is Liquid Group Basic library that supports encryption and decryption using PGP Keys. 

 Example 

>  Encrypt using Public PGP key 
  
 ```csharp
using var pgp = new PgpEncryption(fullPublicKeyPath); 
var contentToEncrypt = "some test message";
var encryptedContent = await pgp.EncryptAsync(contentToEncrypt);

 ```

>  Encrypt using Public PGP key and Sign using Private Key
  
 ```csharp
using var pgp = new PgpEncryption(fullPublicKeyPath, fullPrivateKeyPath, password); 
var contentToEncrypt = "some test message";
var encryptedContent = await pgp.EncryptAndSignAsync(contentToEncrypt);

 ```
